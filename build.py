#!env/bin/python
import sys

from flask import Flask, render_template
from flask_flatpages import FlatPages
from flask_frozen import Freezer
from flask.ext.assets import Environment, Bundle

DEBUG = True
FLATPAGES_AUTO_RELOAD = DEBUG
FLATPAGES_EXTENSION = '.md'

app = Flask(__name__)
app.config.from_object(__name__)
pages = FlatPages(app)
freezer = Freezer(app)
assets = Environment(app)

# ASSETS
css = Bundle('less/main.less',
	filters = 'less, yui_css', output = 'css/main.css')
main_js = Bundle('js/main.js',
	filters = 'yui_js', output = 'js/min/main-min.js')

assets.register('css', css)
assets.register('main_js', main_js)

# ROUTES
@app.route('/')
def index():
	projects = (p for p in pages if 'published' in p.meta and p.path.startswith('projects'))
	projects_sorted = sorted(projects, reverse=True, key=lambda p: p.meta['published'])
	return render_template('index.html', 
		projects = projects_sorted)

# RUN AND BUILD
if __name__ == '__main__':
	if len(sys.argv) > 1 and sys.argv[1] == "build":
		freezer.freeze()
	else:
		app.run(port=8000)